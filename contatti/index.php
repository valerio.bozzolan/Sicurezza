<?php
/* Copyright (C) 2019  Italian Linux Society - http://www.linux.it

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
?>

<?php

require_once ('../funzioni.php');
lugheader ('Contatti');

?>

<div class="container">
	<div class="row">
		<div class="col contacts">
			<p>"Sicurezza" è una iniziativa di <a href="http://www.ils.org/">Italian Linux Society</a>.</p>
			<p>Per contatti relativi alla piattaforma, segnalazioni o commenti, manda una mail a <a href="mailto:webmaster@linux.it">webmaster@linux.it</a>.</p>
			<p>Non esitare a contattarci anche (e soprattutto!) se hai proposte per progetti freesoftware da sottoporre ad un audit di sicurezza. Le segnalazioni della community sono raccolte privatamente, ed al termine della campagna provvederemo a fornirti un report di quanto identificato.</p>
			<p>Il codice sorgente di questo sito è distribuito in licenza <a href="http://www.gnu.org/licenses/agpl-3.0-standalone.html">AGPLv3</a>, e disponibile <a href="http://gitlab.com/ItalianLinuxSociety/Sicurezza">su questa pagina</a>. I contenuti sono pubblicati con licenza <a href="http://creativecommons.org/publicdomain/zero/1.0/deed.en_US">CC-Zero</a></p>
		</div>
	</div>
</div>

<div style="clear: both; margin-bottom: 20px"></div>

<?php lugfooter (); ?>

