<?php
/* Copyright (C) 2019  Italian Linux Society - http://www.linux.it

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
?>

<?php

function conf($name) {
	require('config.php');
	
	if (!isset($$name)) {
		echo 'parametro ' . $name . ' non esistente';
	}

	return $$name;
}

function lugheader ($title, $extracss = null, $extrajs = null) { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="italian" />
	<meta name="robots" content="noarchive" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="https://www.linux.it/shared/?f=bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="https://www.linux.it/shared/?f=main.css" rel="stylesheet" type="text/css" />

	<meta name="dcterms.creator" content="Italian Linux Society" />
	<meta name="dcterms.type" content="Text" />
	<link rel="publisher" href="https://www.ils.org/" />

	<meta name="twitter:title" content="<?php echo conf('website_tagline') ?>" />
	<meta name="twitter:creator" content="@ItaLinuxSociety" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:url" content="<?php echo conf('website_url') ?>/" />
	<meta name="twitter:image" content="<?php echo conf('website_url') ?>/immagini/logo.png" />

	<meta property="og:site_name" content="<?php echo conf('website_name') ?>" />
	<meta property="og:title" content="<?php echo conf('website_name') ?>" />
	<meta property="og:url" content="<?php echo conf('website_url') ?>/" />
	<meta property="og:image" content="<?php echo conf('website_url') ?>/immagini/fb.png" />
	<meta property="og:type" content="website" />
	<meta property="og:country-name" content="Italy" />
	<meta property="og:email" content="webmaster@linux.it" />
	<meta property="og:locale" content="it_IT" />
	<meta property="og:description" content="<?php echo conf('website_tagline') ?>" />

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js"></script>

	<?php

	if ($extracss != null) {
		foreach ($extracss as $e) {
			?>
			<link href="<?php echo $e ?>" rel="stylesheet" type="text/css" />
			<?php
		}
	}

	if ($extrajs != null) {
		foreach ($extrajs as $e) {
			?>
			<script type="text/javascript" src="<?php echo $e ?>"></script>
			<?php
		}
	}

	?>

	<title><?php echo conf('website_name') ?> | <?php echo $title ?></title>
</head>
<body>

<div id="header">
	<img src="/immagini/logo.png" alt="" />
	<div id="maintitle"><?php echo conf('website_name') ?></div>
	<div id="payoff"><?php echo conf('website_tagline') ?></div>

	<div class="menu">
		<a class="generalink" href="/">Home</a>
		<!-- <a class="generalink" href="/progetti/">Progetti Passati</a> -->
		<a class="generalink" href="/contatti/">Contatti</a>

		<p class="social mt-2">
			<a href="https://twitter.com/ItaLinuxSociety"><img src="//www.ils.org/sites/all/themes/linuxday2/images/twitter.png"></a>
			<a href="https://www.facebook.com/ItaLinuxSociety/"><img src="//www.ils.org/sites/all/themes/linuxday2/images/facebook.png"></a>
			<a href="https://gitlab.com/ItalianLinuxSociety/<?php echo conf('website_name') ?>"><img src="//www.ils.org/sites/all/themes/linuxday2/images/gitlab.png"></a>
		</p>
	</div>
</div>

<?php
}

function lugfooter () {
?>

<div id="ils_footer" class="mt-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<span style="text-align: center; display: block">
					<a href="https://www.gnu.org/licenses/agpl-3.0-standalone.html" rel="license">
						<img src="https://www.linux.it/shared/index.php?f=immagini/agpl3.svg" style="border-width:0" alt="AGPLv3 License">
					</a>

					<a href="https://creativecommons.org/publicdomain/zero/1.0/deed.en_US" rel="license">
						<img src="https://www.linux.it/shared/index.php?f=immagini/cczero.png" style="border-width:0" alt="Creative Commons License">
					</a>
				</span>
			</div>

			<div class="col-md-3">
				<h2>RESTA AGGIORNATO!</h2>
				<script type="text/javascript" src="https://www.linux.it/external/widgetnewsletter.js"></script>
				<div id="widgetnewsletter"></div>
			</div>

			<div class="col-md-3">
				<h2>Amici</h2>
				<p style="text-align: center">
					<a href="https://www.ils.org/info#aderenti">
						<img src="https://www.ils.org/sites/ils.org/files/associazioni/getrand.php" border="0" /><br />
						Scopri tutte le associazioni che hanno aderito a ILS.
					</a>
				</p>
			</div>

			<div class="col-md-3">
				<h2>Network</h2>
				<script type="text/javascript" src="https://www.linux.it/external/widgetils.php?referrer=sicurezza"></script>
				<div id="widgetils"></div>
			</div>
		</div>
	</div>

	<div style="clear: both"></div>
</div>

<!-- Matomo -->
<script type="text/javascript">
	var _paq = window._paq || [];
	_paq.push(['disableCookies']);
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
		var u="//stats.madbob.org/";
		_paq.push(['setTrackerUrl', u+'matomo.php']);
		_paq.push(['setSiteId', '14']);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
	})();
</script>
<noscript><p><img src="//stats.madbob.org/matomo.php?idsite=14&amp;rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Matomo Code -->

</body>
</html>

<?php
}

function printableDate($date) {
	list($year, $month, $day) = explode('-', $date);
	return sprintf('%s/%s/%s', $day, $month, $year);
}

function activeProject() {
	$projects = json_decode(file_get_contents('projects.json'));
	$project = $projects[0];

	$now = time();
	$closing = strtotime($project->closing);
	$datediff = $now - $closing;
	$days = floor($datediff / (60 * 60 * 24));

	if ($days > 0)
		return null;
	else
		return $project;
}

