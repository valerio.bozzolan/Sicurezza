<?php
/* Copyright (C) 2019  Italian Linux Society - http://www.linux.it

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
?>

<?php

session_start();

require_once ('funzioni.php');
lugheader ('Home');

?>

<div class="container main-contents">

	<?php

	if (isset($_POST['submitted'])) {
		$wrong = false;
		$project = activeProject();

		if (is_null($project)) {
			$wrong = true;
			$message = 'Non ci sono sessioni di audit attualmente in corso!';
		}
		else {
			$fields = [
				'project' => ['mandatory'],
				'email' => ['email'],
				'name' => ['mandatory'],
				'contents' => ['mandatory'],
				'guide' => ['mandatory'],
				'patch' => [],
				'privacy' => ['mandatory'],
				'verify' => ['mandatory', 'number'],
			];

			$row = [];

			foreach($fields as $f => $options) {
				if (in_array('mandatory', $options) && (!isset($_POST[$f]) || empty(trim($_POST[$f])))) {
					$wrong = true;
					$message = 'Non hai compilato tutti i campi obbligatori!';
					break;
				}

				$row[] = preg_replace("/\s+|[[:^print:]]/", "", trim($_POST[$f]));
			}

			if ($wrong === false) {
				if ($project->name != $_POST['project']) {
					$wrong = true;
					$message = 'Non hai compilato tutti i campi obbligatori!';
				}
			}

			if ($wrong === false) {
				$verify = (int) trim($_POST['verify']);
				if (!isset($_SESSION['captcha_solution']) || $verify != $_SESSION['captcha_solution']) {
					$wrong = true;
					$message = 'Non hai compilato tutti i campi obbligatori!';
				}
			}
		}

		?>

		<div class="row">
			<div class="col">
				<br>
				<?php if($wrong == true): ?>
					<div class="alert alert-danger">
						<?php echo $message ?>
					</div>
				<?php else: ?>
					<div class="alert alert-success">
						<?php

						unset($_SESSION['captcha_solution']);
						sleep(1);

						$f = fopen('data/submissions.csv', 'a');
						fputcsv($f, $row);

						?>

						Grazie per la tua segnalazione!<br>
						Tieniti aggiornato, su questo o su altri progetti futuri, sottoscrivendo la <a href="http://www.ils.org/newsletter">newsletter di Italian Linux Society</a> o seguendoci su <a href="https://twitter.com/italinuxsociety">Twitter</a> o <a href="https://www.facebook.com/italinuxsociety">Facebook</a>.
					</div>
				<?php endif ?>
			</div>
		</div>

		<?php
	}

	if (!isset($_SESSION['captcha_solution'])) {
		$first = rand(1, 19);
		$second = rand(1, 19);
		$captcha_expression = sprintf('%d + %d =', $first, $second);
		$_SESSION['captcha_solution_first'] = $first;
		$_SESSION['captcha_solution_second'] = $second;
		$_SESSION['captcha_solution'] = $first + $second;
	}
	else {
		$first = $_SESSION['captcha_solution_first'];
		$second = $_SESSION['captcha_solution_second'];
	}

	$captcha_expression = sprintf('%d + %d =', $first, $second);

	$project = activeProject();

	?>

	<div class="row">
		<div class="col intro">
			<div class="row">
				<div class="col-9 align-self-center">
					<p>
						Il software libero è sinonimo di privacy e protezione, ma talvolta non basta che il codice sia aperto e consultabile: vulnerabilità e difetti possono essere presenti ed essere sfruttati per accedere a dati privati, a volte sensibili e critici.<br>
						Obiettivo di <strong>Sicurezza.Linux.it</strong> è aiutare i progetti opensource meno noti ad elevare i propri standard di sicurezza e tutela, promuovendo buone pratiche di sviluppo e manutenzione e facendo al contempo da banco di prova per coloro che vogliono acquisire esperienza nell'ambito dell'information security.<br>
						Contribuisci anche tu a migliorare il software libero!
					</p>
				</div>
				<div class="col-3 align-self-center">
					<img class="img-fluid" src="/immagini/icona.png">
				</div>
			</div>
		</div>
	</div>

	<?php if (!is_null($project)): ?>

		<div class="row">
			<div class="col">
				<h2>Progetto in Corso: <?php echo $project->name ?></h2>
				<?php echo str_replace("\n", "</p><p>", $project->descr) ?>
			</div>
		</div>

		<hr>

		<div class="row">
			<div class="col">
				<h3>Come partecipare</h3>
				<ul>
					<li>Consulta <a href="<?php echo $project->url ?>">l'istanza pubblica accessibile qui</a>, e <strong>fai del tuo meglio</strong> per violarla: testa tutti gli input, manipola i dati inviati al server, forgia URL, accedi a informazioni cui non dovresti poter accedere!</li>
					<li>Hai trovato una vulnerabilità? <strong>Compila il form</strong> qui sotto per segnalarla!</li>
					<li>Meglio ancora: consulta <a href="<?php echo $project->repository ?>">il repository accessibile qua</a> e <strong>scrivi una patch</strong> per correggere il problema, oppure fornisci indicazioni su come poterla correggere e mitigare!</li>
					<li>Al termine di questa sessione di audit (<strong>scadenza: <?php echo printableDate($project->closing) ?></strong>), verrà redatto un report destinato agli sviluppatori e verrà pubblicato su questo sito l'elenco di coloro che hanno partecipato col relativo numero di vulnerabilità segnalate e corrette.</li>
				</ul>
			</div>
		</div>

		<hr>

		<div class="row justify-content-center">
			<div class="col-8">
				<form method="POST" action="index.php">
					<input type="hidden" name="project" value="<?php echo $project->name ?>">
					<input type="hidden" name="submitted" value="true">

					<div class="form-group row">
						<label for="name" class="col-sm-2 col-form-label">Nome</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="name" name="name" required>
							<small>Il tuo nome o il tuo nick. Se invii più segnalazioni accertati di usare sempre lo stesso nome.</small>
						</div>
					</div>

					<div class="form-group row">
						<label for="email" class="col-sm-2 col-form-label">Email</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" id="email" name="email">
							<small>Opzionale, per eventuali contatti di approfondimento.</small>
						</div>
					</div>

					<div class="form-group row">
						<label for="contents" class="col-sm-2 col-form-label">Descrizione</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="contents" name="contents" required></textarea>
							<small>Qualche parola introduttiva sul problema riscontrato: tipologia, impatto, possibili cause.</small>
						</div>
					</div>

					<div class="form-group row">
						<label for="guide" class="col-sm-2 col-form-label">Riproduzione</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="guide" name="guide" required></textarea>
							<small>Indicazioni passo per passo per riprodurre il problema.</small>
						</div>
					</div>

					<div class="form-group row">
						<label for="patch" class="col-sm-2 col-form-label">Pull request / patch proposta</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="patch" name="patch">
							<small>Opzionale, ma molto apprezzata. Ne viene tenuto conto per stilare la classifica dei partecipanti!</small>
						</div>
					</div>

					<div class="form-group row">
						<label for="verify" class="col-sm-2 col-form-label"><?php echo $captcha_expression ?></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="verify" name="verify" required>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-2">Privacy</div>
						<div class="col-sm-10">
							<div class="form-check">
								<input class="form-check-input" type="checkbox" id="privacy" name="privacy" required>
								<label class="form-check-label" for="privacy">
									Ho letto ed accetto l'<a href="https://www.ils.org/privacy" target="_blank">informativa sulla privacy</a>
								</label>
							</div>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-10 offset-sm-2">
							<button type="submit" class="btn btn-primary">Invia</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	<?php else: ?>
		<div class="container">
			<div class="row">
				<div class="col">
					<p>
						Non sono al momento attive campagne di security audit.
					</p>
					<p>
						<a href="/progetti">Consulta qui i progetti passati</a>, <a href="/contatti">segnalaci un nuovo progetto libero</a> da sottoporre ad audit, o sottoscrivi <a href="http://www.ils.org/newsletter">la newsletter di Italian Linux Society</a> per futuri aggiornamenti e per ricevere informazioni sulle prossime iniziative.
					</p>
				</div>
			</div>
		</div>
	<?php endif ?>
</div>

<div style="clear: both; margin-bottom: 20px"></div>

<?php lugfooter (); ?>

